# Spring MVC #


## Простой проект на framework Spring ##

### Цель: ###

* Изучение базового функционала Spring MVC;


### Функционал: ###

* Вводим/ храним/ модифицируем/ удаляем  информацию о user'ах;


### Технологии: ###

 * bacend: 
##### java, Maven, SpringMVC, Tomcat, MySQL; #####
 
 * front:
##### формы, Freemarker; #####


#### Ссылка на репу: ####
* [link](git clone https://yury-connect@bitbucket.org/yury-connect/spring_mvc_v20200404.git)

* [link for clone](git@bitbucket.org:yury-connect/spring_mvc_v20200404.git)



### [по материалам вебинара](https://youtu.be/DclLJt8zDWk) ###

[похожая реализация](https://github.com/kliakhin/firstTry)
