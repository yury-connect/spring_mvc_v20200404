package com.vebinar.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;


// # 2
@Configuration   // конфигурация спринга
@ComponentScan(basePackages = {"com.vebinar.service", "com.vebinar.dao"}) // перечислим все пакеты, где лежат компоненты (bean)
public class SpringConfig {

    // для того, чтобы UserDao создался на этапе запуска/ инициализации нашего приложения опишем такие бины:
    // будем использовать Spring jdbc
    @Bean
    public JdbcTemplate getJdbcTemplate() {
        // параметром принимает  DataSource, это тот объект, который будет описывать connection к нашей базе данных
        JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
        return jdbcTemplate;
    }

    // DataSource - это интерфейс
    // DriverManagerDataSource - одна из его реализаций
    // этот бин создаст настройки, чтобы покл. к БД
    @Bean
    public DataSource getDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource(); // javax.sql.DataSource
        dataSource.setUrl("jdbc:mysql://localhost:3306/vebinar20200400?useSSL=false&serverTimezone=UTC&useLegacyDatetimeCode=false");
        dataSource.setUsername("root");
        dataSource.setPassword("1234");
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        return dataSource;
    }

}
