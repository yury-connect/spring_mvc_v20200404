package com.vebinar.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;


// #1
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.vebinar.controller")   // скажем, где лежат контроллеры
public class WebConfig extends WebMvcConfigurerAdapter {   // сконфигурим компонент, который будет искать наши вьюшки в папке WEB-INF

    // определим   ViewResolver
    @Bean
    public ViewResolver getViewResolver() {
        FreeMarkerViewResolver freeMarkerViewResolver = new FreeMarkerViewResolver();
        freeMarkerViewResolver.setOrder(1);   // порядок его загрузке
        freeMarkerViewResolver.setSuffix(".ftl");   // в контроллере будем возвращать только имя
        freeMarkerViewResolver.setPrefix("");   // префикс нам ненужен т.к. у него есть FreeMarkerConfigurer
        return freeMarkerViewResolver;
    }

    @Bean
    public FreeMarkerConfigurer getFreeMarkerConfigurer() {
        FreeMarkerConfigurer freeMarkerConfigurer = new FreeMarkerConfigurer();
        freeMarkerConfigurer.setTemplateLoaderPaths("/", "/WEB-INF/views/"); // несколько путей, где будут лежать наши template
        return freeMarkerConfigurer;

    }

}
