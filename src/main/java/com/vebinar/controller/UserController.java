package com.vebinar.controller;

import com.vebinar.entity.User;
import com.vebinar.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


// в контроллере используем только   service. не перепрыгиваем и не используем напрямую уровень   DAO
@Controller
@RequestMapping("/") // будет обрабатывать весь mapping начиная со "/"    - для т ого, чтобы мы вернули hello
public class UserController {   // есть отображение, за которое отвечает контроллер, обюрабатывает запросы.

    @GetMapping("/hello")   // метод обрабатывает GET запрос по адресу   /hello
    public String hello() {   // название того template кот мы создали  в папке views
        return "hello";
    }
// UserController может обработать GET запрос и на результат он возвращает String "hello"
// После чего подключится   ViewResolver   и увидит, что в WebConfig есть view, кот. лежат в ("/", "/WEB-INF/views/")
// ViewResolver   посмотрел, прилепил к названию   .ftl   и вернул нам   эту html - страницу


    @GetMapping("/")   // чтобы страница  "index" показывалась   (мапинг будет пустой)
    public String index() {   // название того template кот мы создали  в папке views
        return "index";
    }

    // будем использовать написанный service в контроллере
    // т.е. дергнуть какой-то элемент template, который вернет нам всех user из БД
    @Autowired()
    public UserService userService;   // здесь мы инжектим интерфейс

    @GetMapping("/users") // этот Mapping будет работать по запросу /users
    public String getAllUser(Model  model) {
        // Model - это mapa. которая нужна для передачи данных между  view  и нашим контроллером/ приложением. С помощью этой map мы передадим всех User
        model.addAttribute("users", userService.findAll());
        return "usersList";   // метод вернет List<User>
        // после чего наша задача - атрибут с именем "users" обработать в template   "usersList"
        // добавим новый .html в нашем  view  и переименуем в .ftl
    }

    // обработаем эту "<td><a href="/user/${user.id}">${user.id}</a></td>"   ссылку   view
    @GetMapping("/user/{id}")   // id   придет как параметр в этот метод
    public  String getById(@PathVariable("id") int id, Model model) {
        // id кот.придет в URL нужно исполтзовать в нашем методе под именем  id
        model.addAttribute("user", userService.getById(id));
        return "showUser";   // после чего создадим  templste  showUser   в папке  views
    }

    // сделаем форму добавления нового пользователя
    @PostMapping("/addUser")   // будем передавать методом POST
    public String addUser(@ModelAttribute("user")User user) {
        userService.save(user);   //  мы получаем user из формы и дальше он у нас сохраняется
        return "redirect:/users";  // редиректим на страницу всех юзеров
    }   // после чего добавляем  template , который будет создавать нового юзера
    // для того, чтобы попасть в этот метод, сделаем такой же метод, но   GET
    @GetMapping("/addUser")
    public String createUserPage() {
        return "createUser";
    }   // после чего сделаем страничку для добавления нового пользователя  createUser.ftl


    @GetMapping("/delete/{id}")   // id   придет как параметр в этот метод
    public String deleteUser(@PathVariable("id") int id) {
        userService.delete(id);
        return "redirect:/users";  // редиректим (вернем) на страницу всех юзеров
    }

    @GetMapping("/update/{id}")   // id   придет как параметр в этот метод
    public String update(@PathVariable("id") int id, Model model) {
        // вернрет страницу, на которой появится форма с заполненными полями для этого  user
        model.addAttribute("user", userService.getById(id));
        return "editUser";  // редиректим (вернем) на страницу всех юзеров
    }

    @PostMapping("/updateUser")   // будет очень похоже на  вышеописанный метод:
    // @PostMapping("/addUser")
    // public String addUser(@ModelAttribute("user")User user) {  ... слегка подправим
    public String updateUser(@ModelAttribute("user")User user) {
        userService.update(user);
        return "redirect:/user/" + user.getId();
    }

}

