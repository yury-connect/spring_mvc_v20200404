package com.vebinar.dao;

import com.vebinar.entity.User;

import java.util.List;


// 2.1  начнем с DAO прикрутим базу данных
// опишем методы, через которые будем доступаться
// к нашей базе данных
// Будем работать через интерфейс, реализация нам неважна
public interface UserDao {

    List<User> findAll();

    void save(User user);

    User getById(int id);

    void update(User user);

    void  delete(int id);

}
