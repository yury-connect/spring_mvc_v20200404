package com.vebinar.mapper;

import com.vebinar.entity.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


// Тут мы говорим, как сконвертить данные с таблицу в наш  java - объект
public class UserMapper implements RowMapper<User> {


    // простой такой mapper, который будет мапить нашего юзера из ДБ, вручную
    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt("id"));
        user.setName(resultSet.getString("name"));
        user.setEmail(resultSet.getString("email"));
        user.setAge(resultSet.getInt("age"));
        return user;
    }

}
