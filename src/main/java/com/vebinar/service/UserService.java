package com.vebinar.service;

import com.vebinar.entity.User;

import java.util.List;

// в сервисе будет логика/ СЕРВИС НУЖЕН ДЛЯ ТОГО, ЧТОБЫ РАЗДЕЛИТЬ ЛОГИКУ
public interface UserService {

    // UserService точно так же будет доставать листь User -ов
    // пока никакой логики особо не будет, но в дальнейшем она может появиться.
    // ВСЮ ЛОГИКУ ИЗ КОНТРОЛЛЕРОВ И ИЗ DAO НУЖНО ВЫНОСИТЬ В СЕРВИСЫ
    // вся логика должна быть в этом слое приложения
    List<User> findAll();

    // после заполнения методов м слое UserDao перенесем те-же методы  сюда

    void save(User user);

    User getById(int id);

    void update(User user);

    void  delete(int id);

}
