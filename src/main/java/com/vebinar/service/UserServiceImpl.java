package com.vebinar.service;

import com.vebinar.dao.UserDao;
import com.vebinar.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


// В отличии от DAO, где мы использовали JdbcTemplate, тут мы будем уже использовать DAO
@Service   // если  Service  -то тут мож.быть бизнес-логика (валидации, проверки)
//@Component   // вместо  @Service  можно написать  @Component
public class UserServiceImpl implements UserService{

    // нужно заинже5ктить тут наш интерфейс
    @Autowired
    public UserDao userDao;   // это всего лишь интерфейс, его нужно описать в SpringConfig, где рассказать как работать с объектами данного типа

    @Override
    public List<User> findAll() {
        return userDao.findAll();   // чтобы нашло UserDao нужно описать в SpringConfig // getUserDao
    }

    @Override
    public void save(User user) {
        // тут можно сделать проверки прилетающих данныйх
        if (user.getName().length() == 0) throw new IllegalArgumentException();
        userDao.save(user);
    }

    @Override
    public User getById(int id) {
        return userDao.getById(id);
    } // далее напишем   template   на этот   getById

    @Override
    public void update(User user) {
        userDao.update(user);
    }

    @Override
    public void delete(int id) {
        userDao.delete(id);
    }

}
