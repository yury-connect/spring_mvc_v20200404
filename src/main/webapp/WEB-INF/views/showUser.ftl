<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>User info page </title>
</head>
<body>
<h1>User info</h1>
<table border="2">
    <tr>
        <td>Id</td>
        <td>${user.id}</td>
    <#-- user   -это то, что мы  в методе  getById  контроллера   UserController   засунули в модель. которую хотим передать на наш  template-->
    </tr>
    <tr>
        <td>Name</td>
        <td>${user.name}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>${user.email}</td>
    </tr>
    <tr>
        <td>Age</td>
        <td>${user.age}</td>
    </tr>

</table>

<br>
<a href="/users">Back</a> <#--ссылка назад-->
</body>
</html>