<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>User list page </title>
</head>
<body>
<h1>Users list</h1>
<#--тут мы должны вывести список user 'ов,  -->
<#--пример использования:   https://o7planning.org/ru/11251/spring-mvc-and-freemarker-tutorial-->
<table border="1">
    <#--<table>-->
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Email</th>
        <th>Age</th>
    </tr>
<#--будем использовать freeMarker-->
<#--Делаем лист users говорим, что users атрибут, который придет с модели-->
<#--list users использовать как user, т.е. будем работать как с одним объектом-->
<#--следующие ряды будут повторяться столько раз, сколько придет этих user с контроллера, нужно обратиться к этим атрибутам -->
    <#list users as user>
    <tr>
        <#--<td>${user.id}</td>-->
        <td><a href="/user/${user.id}">${user.id}</a></td>
            <#--обработаем этот запрос   "/user/${user.id}">${user.id}    в методе   UserController   -->
            <#--"/user/1" -1-й user, "/user/2" -2-й user-->
        <td>${user.name}</td>
        <td>${user.email}</td>
        <td>${user.age}</td>

        <#--добавим еще 2 кнопки-->
        <td><a href="/delete/${user.id}">Delete</a></td>
        <td><a href="/update/${user.id}">Update</a></td>

    </tr>
    </#list>
</table>

<a href="/addUser">Create new user</a> <#--ссылка добавить нового-->

</body>
</html>